import * as vscode from "vscode";
import {
  lstatSync,
  mkdirSync,
  readFileSync,
  readdirSync,
  writeFileSync,
} from "fs";
import { resolve, dirname } from "path";

export function activate(context: vscode.ExtensionContext) {
  console.log('Congratulations, your extension "templ8r" is now active!');

  let disposable = vscode.commands.registerCommand(
    "templ8r.generate",
    async (uri: vscode.Uri) => {
      const isFolder = lstatSync(uri.fsPath).isDirectory();
      const target = isFolder ? uri.fsPath : dirname(uri.fsPath);

      const name = await vscode.window.showInputBox({
        placeHolder: "Component name",
        prompt: "Enter the name of the component",
      });

      if (!name) {
        return;
      }

      const location = resolve(target, name);

      try {
        scaffold("react-component", target, name);
      } catch (error) {
        console.error(error);
      }

      vscode.window.showInformationMessage(`Template generated at ${location}`);
    }
  );

  context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() {}

function scaffold(template: string, target: string, name: string) {
  const templateFolder = resolve(
    __dirname,
    `./templates/${template}/[componentName]`
  );

  const read = (file: string) =>
    readFileSync(resolve(templateFolder, file), { encoding: "utf-8" });

  const folder = resolve(target, name);
  mkdirSync(folder);

  const files = readdirSync(resolve(__dirname, templateFolder));

  for (const file of files) {
    const content = read(file);

    writeFileSync(
      resolve(folder, file.replace("[componentName]", name)),
      content.replaceAll("[componentName]", name)
    );
  }
}
