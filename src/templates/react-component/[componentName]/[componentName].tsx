import { useTranslation } from '@/hooks';

import { Root } from './styled';

type Props = {
  id: string;
};

export default function [componentName](props: Props) {
  const { id } = props;
  const { t } = useTranslation();
  return <Root>{t(`component:label${id}`)}</Root>;
}
